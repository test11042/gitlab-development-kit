# How to use GitLab Development Kit

See the [top level README](../../README.md#getting-started) for GDK
installation instructions.

Main purpose of GitLab Development Kit is to make GitLab development easier.
Please see [GitLab Contributor documentation](https://docs.gitlab.com/ee/development/README.html)
to learn how to contribute to GitLab.

## Basic

- [Browse your development GitLab server](browse.md)
- [GitLab developer documentation](https://docs.gitlab.com/ee/development/README.html)
- [GDK commands](../gdk_commands.md)
- [Installation, configuration, and development troubleshooting](../troubleshooting.md)
- [Cheat sheet](../../HELP): `gdk help`

## Tips and tricks

- [GitLab performance metrics](performance_metrics.md)
- [Local network binding](local_network.md)
- [Load testing](load_testing.md)
- [Configuration](../configuration.md)

## Special topics

- [Asset Proxy / Camo Server](asset_proxy.md)
- [Database load balancing](database_load_balancing.md)
- [Debugging with Pry](pry.md)
- [Dependency Proxy](dependency_proxy.md)
- [Elasticsearch](elasticsearch.md)
- [Email](email.md)
- [Enable Shell completion](shell_completion.md)
- [End to End Test Configuration](end_to_end_test_configuration.md)
- [GDK in Gitpod](gitpod.md)
- [Git push options](git_push_options.md)
- [GitLab Geo](geo.md)
- [GitLab.com OAuth2](gitlab-oauth2.md)
- [Gitaly and Praefect](gitaly.md)
- [Google OAuth2](google-oauth2.md)
- [HTTPS](nginx.md)
- [Kerberos](kerberos.md)
- [Kubernetes](auto_devops/index.md)
- [LDAP](ldap.md)
- [Lefthook](lefthook.md)
- [NFS](nfs.md)
- [PostgreSQL](postgresql.md)
- [Puma](puma.md)
- [Preview and test the docs site locally](gitlab_docs.md)
- [Preview GitLab changes](preview_gitlab_changes.md)
- [SSH](ssh.md)
- [Serverless (Knative)](serverless.md)
- [Sherlock](sherlock.md)
- [Simulate slow or broken repository storage](simulate_storage.md)
- [Start a Rails console](rails_console.md)
- [Unicorn](unicorn.md)
- [Update external dependencies](update_external_dependencies.md)
- [Use Container Registry with GDK](registry.md)
- [Use GitLab Runner with GDK](runner.md)
- [Use Prometheus with GDK](prometheus/index.md)
